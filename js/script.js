let transactions = [];
let currencyOptions = ['BTC', 'CAD', 'ETH', 'STEEM', 'VTC', 'NEO', 'GRS'];
let currencies = {};
let textFile = null;

document.addEventListener('DOMContentLoaded', function() {

    let stockTypeElement = document.getElementsByClassName('stockType');
    let coinList = document.getElementById('coinList');
    let saveButton = document.getElementById('save');
    let fileInput = document.querySelector('input[type="file"]');

    function makeTextFile(text) {

        if (textFile !== null) window.URL.revokeObjectURL(textFile);

        textFile = window.URL.createObjectURL(new Blob([text], {
            type: 'text/plain'
        }));

        return textFile;
    }

    function loadCurrenciesFromStorage() {

      // Parse the localStorage to JSON
      let savedCurrencies = JSON.parse(localStorage.getItem('currencies')) || {};

      // Lets empty out the coin list elements
      while (coinList.firstChild) {
          coinList.removeChild(coinList.firstChild);
      }

      for (let key in savedCurrencies) {
        let savedCurrency = savedCurrencies[key];
        let currency = getCurrency(savedCurrency.name);

        for (transaction of savedCurrency.transactions) {
          currency.addTransaction(new Transaction(transaction));
        }

      }
    }

    function getCurrency(currency) {

      let currencyTable = currencies[currency];

      if (typeof currencyTable === 'undefined') {
          currencyTable = new Currency(currency);
          currencies[currency] = currencyTable;
          coinList.appendChild(currencyTable.html());
      }

      return currencyTable;
    }

    function addCurrenciesToDropdown() {
      for (let i = 0; i < currencyOptions.length; i++ ) {

          let currency = currencyOptions[i];
          let option = document.createElement('option');

          option.value = currency;
          option.innerText = currency;

          stockTypeElement[0].appendChild(option);
          stockTypeElement[1].appendChild(option.cloneNode(true));
      }
    }

    function initFileImportListener() {
      fileInput.addEventListener('change', function(){
          let reader = new FileReader();

          reader.onload = function() {
              localStorage.setItem('currencies', reader.result);
              loadCurrenciesFromStorage();
          }

          reader.readAsText(fileInput.files[0]);
      });
    }

    // We need to get the highest currency to convert from.
    // As of 2018 it's Fiat -> BTC / ETH
    function getHighestPriorityCurrency(currencies) {

      let priorities = {
        1: ["CAD", "USD", "EUR", "AUD"],
        2: ["BTC", "ETH"]
      }

      let highestPriority = null;

      currencies.forEach(function(currency) {
        for (let priority in priorities) {
          if (priorities[priority].indexOf(currency) >= 0 && ( highestPriority === null || priority < highestPriority.priority)) {
            highestPriority = {
              priority: priority,
              currency: currency
            }
          }
        }
      });

      return highestPriority.currency;
    }

    function initScroller() {

      let transactionForm = document.getElementById('addTransactionForm');

      window.addEventListener('scroll', function(e) {
          transactionForm.parentElement.classList.toggle('fixedToTop', (e.pageY > 255));
      });
    }

    function initAddTransactionListener() {
      document.getElementById('addTransactionForm').addEventListener('submit', function(e) {

          e.preventDefault();

          let txDate = this.querySelector('input[type="date"]').value;
          let txTime = this.querySelector('input[type="time"]').value.split(':');
          let timestamp = (new Date(txDate.replace(/-/g, '/'))).setHours(txTime[0], txTime[1]);

          let txType = this.querySelector('select[name="type"]').value;
          let transactionID = Math.round(Math.random() * timestamp);
          let currencyOne = document.querySelector('#stockOneType').value;
          let currencyTwo = document.querySelector('#stockTwoType').value;
          let highestPriorityCurrency = getHighestPriorityCurrency([currencyOne, currencyTwo]);

          Currency.getPricePerUnit(highestPriorityCurrency, timestamp).then((price) => {

            currencyOne = getCurrency(currencyOne);
            currencyTwo = getCurrency(currencyTwo);

            let currencyOneAmount = document.querySelector('#stockOneAmount').value;
            let currencyTwoAmount = document.querySelector('#stockTwoAmount').value;

            let fmv = price * (currencyOne.name === highestPriorityCurrency ? currencyOneAmount : currencyTwoAmount) ;

            // Currency One
            currencyOne.addTransaction(new Transaction({
              id: transactionID,
              date: txDate,
              time: txTime,
              timestamp: timestamp,
              type: txType,
              commission: currencyOne.name === highestPriorityCurrency ? document.querySelector('input[name="commission"]').value * price : 0.00,
              amount: parseFloat(currencyOneAmount),
              description: `${ (txType === 'SELL' ? 'Sold' : 'Bought') } ${ currencyOneAmount } ${ currencyOne.name } for ${ currencyTwoAmount } ${ currencyTwo.name }`,
              fmv: fmv
            }));

            // Currency Two
            currencyTwo.addTransaction(new Transaction({
              id: transactionID,
              date: txDate,
              time: txTime,
              timestamp: timestamp,
              type: txType === 'SELL' ? 'BUY' : 'SELL',
              commission: currencyTwo.name === highestPriorityCurrency ? document.querySelector('input[name="commission"]').value * price : 0.00,
              amount: parseFloat(currencyTwoAmount),
              description: `${ (txType === 'SELL' ? 'Bought' : 'Sold') } ${ currencyTwoAmount } ${ currencyTwo.name } for ${ currencyOneAmount } ${ currencyOne.name }`,
              fmv: fmv
            }));

            // Update localStorage and save button
            updateSaveButton();
          });
      });
    }

    function initRemoveTransactionListener() {
      document.addEventListener('removeTransaction', function(e) {
        for (let key in currencies) {
          currencies[key].removeTransaction(e.detail.transactionID);
          if (currencies[key].transactions.length === 0) {
            // Let's remove the currency table from the HTML
            currencies[key].element.parentElement.removeChild(currencies[key].element);

            // Let's remove the currency from the global list
            delete currencies[key];
          }
        }

        // Update our localStorage and save button
        updateSaveButton();
      });
    }

    function updateSaveButton() {
      localStorage.setItem('currencies', JSON.stringify(currencies));
      saveButton.setAttribute('href', makeTextFile(localStorage.currencies));
      saveButton.setAttribute('download', 'CryptoTax_' + Date.now());
    }

    initFileImportListener();
    initAddTransactionListener();
    initRemoveTransactionListener();

    loadCurrenciesFromStorage();
    updateSaveButton();

    initScroller();

    addCurrenciesToDropdown();

});
