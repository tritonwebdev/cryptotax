class AJAX {

    constructor(options){
        for (var key in options) {
            if (options.hasOwnProperty(key)) {
                if (key === 'success') {
                    this.onSuccess = options[key];
                }
                else if (key === 'fail') {
                    this.onFail = options[key];
                }
                else {
                    this[key] = options[key];
                }
            }
        }
    }

    static send(options) {
        return new AJAX(options).send();
    }

    send() {

        let xhr = new XMLHttpRequest(),
            _this = this;

        xhr.onreadystatechange = function() {

            if (this.readyState === 4) {

                switch (this.status) {

                    case 200:
                        _this.onSuccess(this.responseText);
                        break;

                    case 404:
                        _this.onFail(this.responseText);
                        break;

                    case 500:
                        _this.onFail(this.responseText);
                        break;
                }

            }

        };

        xhr.open(_this.method, _this.url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        xhr.send(_this.JSON_to_URLEncoded(_this.data));
    }

    JSON_to_URLEncoded(element, key = null, list = []){

        if(typeof(element) == 'object'){
            for (let idx in element)
                this.JSON_to_URLEncoded(element[idx], key ? key + '['+idx+']' : idx, list);
        } else {
            list.push(key+'='+encodeURIComponent(element));
        }

        return list.join('&');
    }


}
