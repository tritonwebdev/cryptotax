class Transaction {

  constructor(options) {

    let self = this;
    self.timestamp = options.timestamp || '';
    self.time = options.time || '';
    self.date = options.date;
    self.id = options.id;
    self.type = options.type;
    self.amount = options.amount;
    self.description = options.description;
    self.fmv = options.fmv
    self.commission = parseFloat(options.commission);
  }

}
