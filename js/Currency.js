class Currency
{
    constructor(name) {
      this.element = document.createElement('table');

      this.name = name;
      this.transactions = [];
    }

    addTransaction(transaction) {
      this.transactions.push(transaction);
      this.refresh();
    }

    removeTransaction(transactionID) {
      var self = this;

      self.transactions.forEach(function(transaction, idx) {
        if (transaction.id === transactionID) {
          self.transactions.splice(idx, 1);
          self.refresh();
        }
      });

    }

    html() {

      let thead = this.makeHeader();
      let tbody = this.makeBody();

      this.element.id = this.name;
      this.element.classList.add('table', 'is-fullwidth');

      this.element.appendChild(thead);
      this.element.appendChild(tbody);

      return this.element;
    }

    makeHeader() {

      let thead = document.createElement('thead');

      thead.innerHTML = `
        <tr>
          <th class="has-background-link has-text-white" colspan="10">${ this.name }</th>
        </tr>
        <tr>
          <th>Date</th>
          <th>Description</th>
          <th>Units</th>
          <th>FMV</th>
          <th>Expenses</th>
          <th>Gain / Loss</th>
          <th>ACB</th>
          <th>Balance</th>
          <th></th>
        </tr>
      `;

      return thead;
    }

    makeBody() {

      let self = this;
      let tbody = document.createElement('tbody');
      let editTxModal = document.querySelector('#editTransactionModal');

      let acb = 0.00;
      let balance = 0.00;

      // Sort our transactions by timestamp
      self.transactions.sort(function(a, b) {
        return a.timestamp - b.timestamp;
      });

      // Let's go through and render our transactions
      self.transactions.forEach(function(tx) {

        // Let's check if we are working with a Transaction object
        if (typeof tx === 'object' && tx.constructor.name === 'Transaction') {

          // Initalize our table row to hold our transaction information
          let tr = document.createElement('tr');

          // Add our type as a class for styling
          tr.classList.add(tx.type.toLowerCase());

          // Initialize our gain at 0.00.
          let gain = 0.00;

          if (tx.type === 'BUY') {

            // Since we are buying, let's increase our balance and ACB.
            balance += tx.amount;
            acb += tx.fmv + tx.commission;

          } else {

            /*
              We divide the adjusted cost base (ACB) by the balance which gives
              us an average adjusted cost base. If there isn't a previous ACB,
              probably because it's FIAT, we return a value of 1.00.
            */

            let avgACB = (acb === 0 && balance === 0) ? 1.00 : acb / balance;

            /*
              We need to get the adjusted cost base for this transaction. This
              will be compared against the fair market value (FMV) to see if we
              made capital gains.
            */

            let txACB = tx.amount * avgACB;

            /*
              To determine our capital gains for this transaction we need to
              subtract the adjusted base cost for this tranasaction from the
              fair market value.
            */
            gain = tx.fmv - txACB - tx.commission;


            // Since we are selling, let's deduct the txAmount from the balance.
            balance -= tx.amount;

            // Let's deduct the transaction adjusted cost base from our current acb.
            acb -= txACB;
          }

          tr.innerHTML = `
            <td>${ tx.date } @ ${ tx.time.join(':') }</td>
            <td>${ tx.description }</td>
            <td>${ tx.amount }</td>
            <td>${ (tx.type === 'SELL' ? tx.fmv.toFixed(2) : '' ) }</td>
            <td>${ tx.commission }</td>
            <td>${ (tx.type === 'SELL' ? gain.toFixed(2) : '') }</td>
            <td>${ acb.toFixed(2) }</td>
            <td>${ balance.toFixed(10) }</td>
          `;

          let optionsCell = document.createElement('td');
          optionsCell.innerHTML += `<button class="options button is-danger is-invisible">Delete</button>`;

          tr.appendChild(optionsCell);

          let deleteButton = optionsCell.querySelector('.options.is-danger');

          tr.addEventListener('mouseenter', function() {
            deleteButton.classList.remove('is-invisible');
          });

          tr.addEventListener('mouseleave', function() {
            deleteButton.classList.add('is-invisible');
          });

          deleteButton.addEventListener('click', function() {
            let event = new CustomEvent("removeTransaction", {
              detail: {
                transactionID: tx.id
              }
            });

            document.dispatchEvent(event);
          });

          tbody.appendChild(tr);
        }
      });

      return tbody;
    }

    refresh() {
        this.element.querySelector('tbody').replaceWith(this.makeBody());
    }

    static getPricePerUnit(currency, timestamp) {
      return new Promise((resolve, reject) => {
        AJAX.send({
            method: 'GET',
            url: `https://min-api.cryptocompare.com/data/pricehistorical?fsym=${ currency }&tsyms=CAD&ts=${ timestamp }`,
            success: function(res) {
              let data = JSON.parse(res);
              resolve(data[currency]['CAD']);
            }
        });
      });
    }
}
